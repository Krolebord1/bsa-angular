import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'enum'})
export class EnumToArrayPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    return Object.keys(value)
      .filter(x => !isNaN(+x))
      .map(x => { return {
        index: +x,
        name: value[x]
      }})
  }
}
