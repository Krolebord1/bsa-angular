import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'formattedDate'})
export class FormattedDatePipe implements PipeTransform {
  transform(value: Date | null | undefined | string, ...args: any[]): any {
    if(!value)
      return '';

    const date: Date = typeof value === 'string'
      ? new Date(Date.parse(value))
      : value;

    return `${date.getDate()} ${FormattedDatePipe.getMonthName(date)} ${date.getFullYear()}`;
  }

  private static getMonthName(date: Date) {
    return date.toLocaleString('default', { month: 'long' })
  }
}
