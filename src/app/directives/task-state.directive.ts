import { Directive, ElementRef, Input, Renderer2 } from "@angular/core";
import { TaskStateEnum } from "../models/tasks/task-state.enum";

@Directive({
  selector: '[taskState]'
})
export class TaskStateDirective {
  @Input() taskState!: TaskStateEnum;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    switch (this.taskState) {
      case TaskStateEnum.planned:
        this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'antiquewhite');
        break;
      case TaskStateEnum.started:
        this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'aquamarine');
        break;
      case TaskStateEnum.finished:
        this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'yellowgreen');
        break;
      case TaskStateEnum.cancelled:
        this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'orangered')
        break;
    }
  }
}
