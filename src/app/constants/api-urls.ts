import { CrudControllerUrls } from "../models/crud-controller-urls";

export const ApiScheme = "https";
export const ApiAuthority = "localhost:5001";
export const ApiVersion = "api";

export const ApiPath = `${ApiScheme}://${ApiAuthority}/${ApiVersion}`;

export const ApiUsers = new CrudControllerUrls("users");

export const ApiProjects = new CrudControllerUrls("projects");

export const ApiTeams = new CrudControllerUrls("teams");

export const ApiTasks = new CrudControllerUrls("tasks");
