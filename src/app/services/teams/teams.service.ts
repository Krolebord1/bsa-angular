import { Injectable } from "@angular/core";
import { TeamReadDto } from "../../models/teams/team-read.dto";
import { TeamWriteDto } from "../../models/teams/team-write.dto";
import { CrudService } from "../common/crud.service";
import { CrudControllerUrls } from "../../models/crud-controller-urls";
import { ApiTeams } from "../../constants/api-urls";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TeamsService extends CrudService<TeamReadDto, TeamWriteDto> {
  protected getApiUrls(): CrudControllerUrls {
    return ApiTeams;
  }

  constructor(http: HttpClient) {
    super(http);
  }
}
