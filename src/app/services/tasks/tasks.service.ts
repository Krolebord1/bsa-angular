import { Injectable } from "@angular/core";
import { CrudService } from "../common/crud.service";
import { CrudControllerUrls } from "../../models/crud-controller-urls";
import { ApiTasks } from "../../constants/api-urls";
import { HttpClient } from "@angular/common/http";
import { TaskReadDto } from "../../models/tasks/task-read.dto";
import { TaskWriteDto } from "../../models/tasks/task-write.dto";

@Injectable({
  providedIn: 'root'
})
export class TasksService extends CrudService<TaskReadDto, TaskWriteDto> {
  protected getApiUrls(): CrudControllerUrls {
    return ApiTasks;
  }

  constructor(http: HttpClient) {
    super(http);
  }
}
