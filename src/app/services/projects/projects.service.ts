import { Injectable } from "@angular/core";
import { CrudService } from "../common/crud.service";
import { CrudControllerUrls } from "../../models/crud-controller-urls";
import { ApiProjects } from "../../constants/api-urls";
import { HttpClient } from "@angular/common/http";
import { ProjectReadDto } from "../../models/projects/project-read.dto";
import { ProjectWriteDto } from "../../models/projects/project-write.dto";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends CrudService<ProjectReadDto, ProjectWriteDto> {
  protected getApiUrls(): CrudControllerUrls {
    return ApiProjects;
  }

  constructor(http: HttpClient) {
    super(http);
  }
}
