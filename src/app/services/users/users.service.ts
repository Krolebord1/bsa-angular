import { Injectable } from "@angular/core";
import { CrudService } from "../common/crud.service";
import { CrudControllerUrls } from "../../models/crud-controller-urls";
import { ApiUsers } from "../../constants/api-urls";
import { HttpClient } from "@angular/common/http";
import { UserReadDto } from "../../models/users/user-read.dto";
import { UserWriteDto } from "../../models/users/user-write.dto";

@Injectable({
  providedIn: 'root'
})
export class UsersService extends CrudService<UserReadDto, UserWriteDto> {
  protected getApiUrls(): CrudControllerUrls {
    return ApiUsers;
  }

  constructor(http: HttpClient) {
    super(http);
  }
}
