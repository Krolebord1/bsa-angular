import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { CrudControllerUrls } from "../../models/crud-controller-urls";

export abstract class CrudService<TReadDto, TWriteDto> {
  private readonly apiUrls: CrudControllerUrls;

  protected abstract getApiUrls(): CrudControllerUrls;

  protected constructor(private http: HttpClient) {
    this.apiUrls = this.getApiUrls();
  }

  public getAll(): Observable<HttpResponse<TReadDto[]>> {
    return this.http.get<TReadDto[]>(this.apiUrls.GetAll(), { observe: 'response' });
  }

  public getById(id: number): Observable<HttpResponse<TReadDto>> {
    return this.http.get<TReadDto>(this.apiUrls.GetById(id), { observe: 'response' });
  }

  public create(team: TWriteDto): Observable<HttpResponse<object>> {
    return this.http.post(this.apiUrls.PostNew(), team, { observe: 'response' });
  }

  public update(id: number, team: TWriteDto): Observable<HttpResponse<object>> {
    return this.http.put(this.apiUrls.PutUpdate(id), team, { observe: 'response' });
  }

  public delete(id: number): Observable<HttpResponse<object>> {
    return this.http.delete(this.apiUrls.Delete(id), { observe: 'response' });
  }
}
