import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamsListComponent } from "./components/teams/teams-list/teams-list.component";
import { TeamsAddComponent } from "./components/teams/teams-add/teams-add.component";
import { TeamsEditComponent } from "./components/teams/teams-edit/teams-edit.component";
import { UsersListComponent } from "./components/users/users-list/users-list.component";
import { CanDeactivateGuard } from "./guards/can-deactivate.guard";
import { HomeComponent } from "./components/home/home.component";
import { UsersAddComponent } from "./components/users/users-add/users-add.component";
import { UsersEditComponent } from "./components/users/users-edit/users-edit.component";
import { ProjectsListComponent } from "./components/projects/projects-list/projects-list.component";
import { ProjectsAddComponent } from "./components/projects/projects-add/projects-add.component";
import { ProjectsEditComponent } from "./components/projects/projects-edit/projects-edit.component";
import { TasksListComponent } from "./components/tasks/tasks-list/tasks-list.component";
import { TasksAddComponent } from "./components/tasks/tasks-add/tasks-add.component";
import { TasksEditComponent } from "./components/tasks/tasks-edit/tasks-edit.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent, pathMatch: 'full' },

  { path: 'teams', component: TeamsListComponent, pathMatch: 'full' },
  { path: 'teams/add', component: TeamsAddComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full' },
  { path: 'teams/edit/:id', component: TeamsEditComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full'},

  { path: 'users', component: UsersListComponent, pathMatch: 'full' },
  { path: 'users/add', component: UsersAddComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full' },
  { path: 'users/edit/:id', component: UsersEditComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full'},

  { path: 'projects', component: ProjectsListComponent, pathMatch: 'full' },
  { path: 'projects/add', component: ProjectsAddComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full' },
  { path: 'projects/edit/:id', component: ProjectsEditComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full'},

  { path: 'tasks', component: TasksListComponent, pathMatch: 'full' },
  { path: 'tasks/add', component: TasksAddComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full' },
  { path: 'tasks/edit/:id', component: TasksEditComponent, canDeactivate: [CanDeactivateGuard], pathMatch: 'full'},

  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
