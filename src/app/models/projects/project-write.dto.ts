export interface ProjectWriteDto {
  authorId?: number;
  teamId?: number;
  name: string;
  description: string;
  deadline: Date;
}
