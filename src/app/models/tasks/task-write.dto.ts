import { TaskStateEnum } from "./task-state.enum";

export interface TaskWriteDto {
  projectId: number;
  performerId?: number;
  name: string;
  description: string;
  state: TaskStateEnum;
  finishedAt?: Date;
}
