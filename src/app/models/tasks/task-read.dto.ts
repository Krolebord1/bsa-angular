import { TaskStateEnum } from "./task-state.enum";

export interface TaskReadDto {
  id: number;
  projectId: number;
  performerId?: number;
  name: string;
  description: string;
  state: TaskStateEnum;
  createdAt: Date;
  finishedAt?: Date;
}
