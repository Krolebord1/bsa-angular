export enum TaskStateEnum {
  planned,
  started,
  finished,
  cancelled
}
