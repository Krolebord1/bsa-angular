export interface UserWriteDto {
  teamId?: number;
  firstName: string;
  lastName: string;
  email: string;
  birthDay: Date;
}
