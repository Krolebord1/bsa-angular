export interface TeamReadDto {
  id: number;
  name: string;
  createdAt: Date;
}
