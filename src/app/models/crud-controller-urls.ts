import { ApiPath } from "../constants/api-urls";

export class CrudControllerUrls {
  private readonly controllerPath: string;

  constructor(controllerPath: string) {
    this.controllerPath = `${ApiPath}/${controllerPath}`;
  }

  public GetAll(): string {
    return this.controllerPath;
  }

  public GetById(id: number): string {
    return `${this.controllerPath}/${id}`;
  }

  public PostNew(): string {
    return this.controllerPath;
  }

  public PutUpdate(id: number): string {
    return `${this.controllerPath}/${id}`;
  }

  public Delete(id: number): string {
    return `${this.controllerPath}/${id}`;
  }
}
