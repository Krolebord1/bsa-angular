import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsListComponent } from "../components/teams/teams-list/teams-list.component";
import { TeamsAddComponent } from "../components/teams/teams-add/teams-add.component";
import { TeamsFormComponent } from "../components/teams/teams-form/teams-form.component";
import { TeamsEditComponent } from "../components/teams/teams-edit/teams-edit.component";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MatSliderModule } from "@angular/material/slider";
import { MatIconModule } from "@angular/material/icon";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { AppRoutingModule } from "../app-routing.module";
import { TasksListComponent } from '../components/tasks/tasks-list/tasks-list.component';
import { TasksFormComponent } from '../components/tasks/tasks-form/tasks-form.component';
import { MatSelectModule } from "@angular/material/select";
import { AppModule } from "../app.module";
import { AppCommonModule } from "./app-common.module";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { TasksAddComponent } from '../components/tasks/tasks-add/tasks-add.component';
import { TasksEditComponent } from '../components/tasks/tasks-edit/tasks-edit.component';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { TaskStateDirective } from "../directives/task-state.directive";

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    AppCommonModule,
    MatButtonToggleModule,
    MatDatepickerModule
  ],
  declarations: [
    TasksListComponent,
    TasksFormComponent,
    TasksAddComponent,
    TasksEditComponent,
    TaskStateDirective
  ],
  exports: []
})
export class TasksModule { }
