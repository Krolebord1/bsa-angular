import { NgModule } from "@angular/core";
import { EnumToArrayPipe } from "../pipes/enum-to-array.pipe";
import { FormattedDatePipe } from "../pipes/formatted-date.pipe";

@NgModule({
  imports: [],
  declarations: [
    EnumToArrayPipe,
    FormattedDatePipe
  ],
  exports: [
    EnumToArrayPipe,
    FormattedDatePipe
  ]
})
export class AppCommonModule { }
