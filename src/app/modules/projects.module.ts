import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { AppRoutingModule } from "../app-routing.module";
import { ProjectsListComponent } from '../components/projects/projects-list/projects-list.component';
import { ProjectsFormComponent } from '../components/projects/projects-form/projects-form.component';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { ProjectsAddComponent } from '../components/projects/projects-add/projects-add.component';
import { ProjectsEditComponent } from '../components/projects/projects-edit/projects-edit.component';
import { AppCommonModule } from "./app-common.module";
import { MatSelectModule } from "@angular/material/select";

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    AppCommonModule,

    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
  ],
  declarations: [
    ProjectsListComponent,
    ProjectsFormComponent,
    ProjectsAddComponent,
    ProjectsEditComponent
  ],
  exports: []
})
export class ProjectsModule { }
