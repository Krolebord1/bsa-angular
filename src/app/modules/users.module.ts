import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from "../components/users/users-list/users-list.component";
import { AppRoutingModule } from "../app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { MatTableModule } from "@angular/material/table";
import { UsersAddComponent } from '../components/users/users-add/users-add.component';
import { UsersFormComponent } from '../components/users/users-form/users-form.component';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { UsersEditComponent } from '../components/users/users-edit/users-edit.component';
import { AppCommonModule } from "./app-common.module";
import { MatSelectModule } from "@angular/material/select";

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    MatTableModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AppCommonModule,
    MatSelectModule
  ],
  declarations: [
    UsersListComponent,
    UsersAddComponent,
    UsersFormComponent,
    UsersEditComponent
  ],
  exports: []
})
export class UsersModule { }
