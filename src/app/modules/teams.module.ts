import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsListComponent } from "../components/teams/teams-list/teams-list.component";
import { TeamsAddComponent } from "../components/teams/teams-add/teams-add.component";
import { TeamsFormComponent } from "../components/teams/teams-form/teams-form.component";
import { TeamsEditComponent } from "../components/teams/teams-edit/teams-edit.component";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { AppRoutingModule } from "../app-routing.module";
import { AppCommonModule } from "./app-common.module";

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    AppCommonModule,
  ],
  declarations: [
    TeamsListComponent,
    TeamsAddComponent,
    TeamsFormComponent,
    TeamsEditComponent,
  ],
  exports: []
})
export class TeamsModule { }
