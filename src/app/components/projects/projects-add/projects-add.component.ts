import { Component } from '@angular/core';
import { FormViewComponent } from "../../common/form-view.component";
import { ProjectsFormComponent } from "../projects-form/projects-form.component";
import { Router } from "@angular/router";
import { ProjectsService } from "../../../services/projects/projects.service";
import { ProjectWriteDto } from "../../../models/projects/project-write.dto";

@Component({
  selector: 'app-projects-add',
  templateUrl: './projects-add.component.html',
  styleUrls: ['./projects-add.component.scss', '../../../styles/center-card.scss']
})
export class ProjectsAddComponent extends FormViewComponent<ProjectsFormComponent> {
  constructor(
    private router: Router,
    private projectsService: ProjectsService
  ) {
    super();
  }

  goBack(): void {
    this.router.navigateByUrl('projects');
  }

  add(projectDto: ProjectWriteDto): void {
    this.projectsService.create(projectDto)
      .subscribe(
        response => {
          if(response.ok)
            this.goBack();
        }
      );
  }
}
