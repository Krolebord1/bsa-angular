import { Component, OnInit } from '@angular/core';
import { FormViewComponent } from "../../common/form-view.component";
import { ActivatedRoute, Router } from "@angular/router";
import { ProjectWriteDto } from "../../../models/projects/project-write.dto";
import { ProjectReadDto } from "../../../models/projects/project-read.dto";
import { ProjectsFormComponent } from "../projects-form/projects-form.component";
import { ProjectsService } from "../../../services/projects/projects.service";

@Component({
  selector: 'app-projects-edit',
  templateUrl: './projects-edit.component.html',
  styleUrls: ['./projects-edit.component.scss', '../../../styles/center-card.scss']
})
export class ProjectsEditComponent extends FormViewComponent<ProjectsFormComponent> implements OnInit {
  project: ProjectReadDto | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectsService: ProjectsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getProject();
  }

  getProject(): void {
    const routeId = this.route.snapshot.paramMap.get('id');

    if(!routeId) {
      this.project = null;
      return;
    }

    const id: number = +routeId;
    this.projectsService.getById(id).subscribe(
      response => this.project = response.body
    );
  }

  goBack(): void {
    this.router.navigateByUrl("projects");
  }

  save(projectDto: ProjectWriteDto): void {
    if(!this.project) {
      this.goBack();
      return;
    }

    this.projectsService.update(this.project.id, projectDto).subscribe(
      response => {
        if(response.ok) {
          this.goBack();
        }
      }
    );
  }
}

