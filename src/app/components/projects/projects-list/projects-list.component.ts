import { Component } from '@angular/core';
import { ProjectsService } from "../../../services/projects/projects.service";
import { ModelsListComponent } from "../../common/models-list.component";
import { ProjectReadDto } from "../../../models/projects/project-read.dto";
import { CrudService } from "../../../services/common/crud.service";

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: [
    './projects-list.component.scss',
    '../../../styles/center-card.scss',
    '../../../styles/card-list.scss'
  ]
})
export class ProjectsListComponent extends ModelsListComponent<ProjectReadDto> {
  constructor(private projectsService: ProjectsService) {
    super();
  }

  getModelsService(): CrudService<ProjectReadDto, any> {
    return this.projectsService;
  }
}
