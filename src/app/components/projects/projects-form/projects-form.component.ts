import { Component } from '@angular/core';
import { FormComponent } from "../../common/form.component";
import { ProjectWriteDto } from "../../../models/projects/project-write.dto";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { TeamReadDto } from "../../../models/teams/team-read.dto";
import { UserReadDto } from "../../../models/users/user-read.dto";
import { UsersService } from "../../../services/users/users.service";
import { TeamsService } from "../../../services/teams/teams.service";
import { filter, map } from "rxjs/operators";

@Component({
  selector: 'app-projects-form',
  templateUrl: './projects-form.component.html',
  styleUrls: ['./projects-form.component.scss', '../../../styles/form.scss']
})
export class ProjectsFormComponent extends FormComponent<ProjectWriteDto> {
  form: FormGroup = this.formBuilder.group({
    name: [this.writeDto.name, [Validators.required, Validators.maxLength(32)]],
    description: [this.writeDto.description, Validators.required],
    deadline: [this.writeDto.deadline, [Validators.required]],
    authorId: [this.writeDto.authorId],
    teamId: [this.writeDto.teamId]
  });

  users$!: Observable<UserReadDto[]>;
  teams$!: Observable<TeamReadDto[]>;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private teamsService: TeamsService
  ) {
    super();
  }

  override ngOnInit() {
    super.ngOnInit();
    this.users$ = this.usersService.getAll().pipe(
      filter(response => response.ok),
      map(value => value.body!)
    );
    this.teams$ = this.teamsService.getAll().pipe(
      filter(response => response.ok),
      map(value => value.body!)
    );
  }

  getDefaultDto(): ProjectWriteDto {
    return {
      name: "",
      description: "",
      deadline: new Date(Date.now())
    };
  }
}
