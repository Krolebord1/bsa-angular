import { Component, OnInit } from '@angular/core';
import { TeamWriteDto } from "../../../models/teams/team-write.dto";
import { TeamsService } from "../../../services/teams/teams.service";
import { Router } from "@angular/router";
import { FormViewComponent } from "../../common/form-view.component";
import { TeamsFormComponent } from "../teams-form/teams-form.component";

@Component({
  selector: 'app-teams-add',
  templateUrl: './teams-add.component.html',
  styleUrls: ['./teams-add.component.scss']
})
export class TeamsAddComponent extends FormViewComponent<TeamsFormComponent> implements OnInit {
  constructor(
    private router: Router,
    private teamsService: TeamsService
  ) {
    super();
  }

  ngOnInit(): void {
  }

  goBack(): void {
    this.router.navigateByUrl('teams');
  }

  add(teamDto: TeamWriteDto): void {
    this.teamsService.create(teamDto)
      .subscribe(
        response => {
          if(response.ok)
            this.goBack();
        }
      );
  }
}
