import { Component } from '@angular/core';
import { TeamsService } from "../../../services/teams/teams.service";
import { TeamReadDto } from "../../../models/teams/team-read.dto";
import { ModelsListComponent } from "../../common/models-list.component";
import { CrudService } from "../../../services/common/crud.service";

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent extends ModelsListComponent<TeamReadDto> {
  constructor(private teamsService: TeamsService) {
    super();
  }

  getModelsService(): CrudService<TeamReadDto, any> {
    return this.teamsService;
  }
}
