import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TeamsService } from "../../../services/teams/teams.service";
import { TeamReadDto } from "../../../models/teams/team-read.dto";
import { TeamWriteDto } from "../../../models/teams/team-write.dto";
import { FormViewComponent } from "../../common/form-view.component";
import { TeamsFormComponent } from "../teams-form/teams-form.component";

@Component({
  selector: 'app-teams-edit',
  templateUrl: './teams-edit.component.html',
  styleUrls: ['./teams-edit.component.scss']
})
export class TeamsEditComponent extends FormViewComponent<TeamsFormComponent> implements OnInit {
  team: TeamReadDto | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teamsService: TeamsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getTeam();
  }

  getTeam(): void {
    const routeId = this.route.snapshot.paramMap.get('id');

    if(!routeId) {
      this.team = null;
      return;
    }

    const id: number = +routeId;
    this.teamsService.getById(id).subscribe(
      response => this.team = response.body
    );
  }

  goBack(): void {
    this.router.navigateByUrl("teams");
  }

  save(teamDto: TeamWriteDto): void {
    if(!this.team) {
      this.goBack();
      return;
    }

    this.teamsService.update(this.team.id, teamDto).subscribe(
      response => {
        if(response.ok) {
          this.goBack();
        }
      }
    );
  }
}
