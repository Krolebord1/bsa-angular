import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TeamWriteDto } from "../../../models/teams/team-write.dto";
import { FormComponent } from "../../common/form.component";

@Component({
  selector: 'app-teams-form',
  templateUrl: './teams-form.component.html',
  styleUrls: ['./teams-form.component.scss', '../../../styles/form.scss']
})
export class TeamsFormComponent extends FormComponent<TeamWriteDto> {
  form: FormGroup = this.formBuilder.group({
    name: [this.writeDto.name, [Validators.required, Validators.maxLength(32)]]
  });

  constructor(private formBuilder: FormBuilder) {
    super();
  }

  getDefaultDto(): TeamWriteDto {
    return {
      name: ""
    };
  }
}
