import { Component } from '@angular/core';
import { UserReadDto } from "../../../models/users/user-read.dto";
import { UsersService } from "../../../services/users/users.service";
import { ModelsListComponent } from "../../common/models-list.component";
import { CrudService } from "../../../services/common/crud.service";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: [
    './users-list.component.scss',
    '../../../styles/center-card.scss',
    '../../../styles/card-list.scss'
  ]
})
export class UsersListComponent extends ModelsListComponent<UserReadDto> {
  constructor(private usersService: UsersService) {
    super();
  }

  getModelsService(): CrudService<UserReadDto, any> {
    return this.usersService;
  }
}
