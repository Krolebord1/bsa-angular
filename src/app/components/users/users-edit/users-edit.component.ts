import { Component, OnInit } from '@angular/core';
import { FormViewComponent } from "../../common/form-view.component";
import { ActivatedRoute, Router } from "@angular/router";
import { UserReadDto } from "../../../models/users/user-read.dto";
import { UserWriteDto } from "../../../models/users/user-write.dto";
import { UsersService } from "../../../services/users/users.service";
import { UsersFormComponent } from "../users-form/users-form.component";

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss', '../../../styles/center-card.scss']
})
export class UsersEditComponent extends FormViewComponent<UsersFormComponent> implements OnInit {
  user: UserReadDto | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usersService: UsersService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    const routeId = this.route.snapshot.paramMap.get('id');

    if(!routeId) {
    this.user = null;
    return;
  }

  const id: number = +routeId;
    this.usersService.getById(id).subscribe(
      response => this.user = response.body
    );
  }

  goBack(): void {
    this.router.navigateByUrl("users");
  }

  save(userDto: UserWriteDto): void {
      if(!this.user) {
      this.goBack();
      return;
    }

    this.usersService.update(this.user.id, userDto).subscribe(
      response => {
        if(response.ok) {
          this.goBack();
        }
      }
    );
  }
}
