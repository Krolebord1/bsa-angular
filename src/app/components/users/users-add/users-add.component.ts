import { Component } from '@angular/core';
import { UsersService } from "../../../services/users/users.service";
import { UserWriteDto } from "../../../models/users/user-write.dto";
import { FormViewComponent } from "../../common/form-view.component";
import { Router } from "@angular/router";
import { UsersFormComponent } from "../users-form/users-form.component";

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.scss', '../../../styles/center-card.scss']
})
export class UsersAddComponent extends FormViewComponent<UsersFormComponent> {
  constructor(
    private router: Router,
    private usersService: UsersService
  ) {
    super();
  }

  goBack(): void {
    this.router.navigateByUrl('users');
  }

  add(userDto: UserWriteDto): void {
    this.usersService.create(userDto)
      .subscribe(
        response => {
          if(response.ok)
            this.goBack();
        }
      );
  }
}
