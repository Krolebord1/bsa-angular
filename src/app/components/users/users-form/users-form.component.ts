import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserWriteDto } from "../../../models/users/user-write.dto";
import { FormComponent } from "../../common/form.component";
import { TeamsService } from "../../../services/teams/teams.service";
import { Observable } from "rxjs";
import { TeamReadDto } from "../../../models/teams/team-read.dto";
import { filter, map } from "rxjs/operators";

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss', '../../../styles/form.scss']
})
export class UsersFormComponent extends FormComponent<UserWriteDto> implements OnInit {
  form: FormGroup = this.formBuilder.group({
    firstName: [this.writeDto.firstName, [Validators.required, Validators.maxLength(32)]],
    lastName: [this.writeDto.lastName, Validators.required],
    email: [this.writeDto.email, [Validators.required, Validators.email]],
    birthDay: [this.writeDto.birthDay, Validators.required],
    teamId: [this.writeDto.teamId]
  });

  teams$!: Observable<TeamReadDto[]>;

  constructor(
    private formBuilder: FormBuilder,
    private teamsService: TeamsService
  ) {
    super();
  }

  override ngOnInit() {
    super.ngOnInit();
    this.teams$ = this.teamsService.getAll().pipe(
      filter(response => response.ok),
      map(value => value.body!)
    );
  }

  getDefaultDto(): UserWriteDto {
    return {
      firstName: "",
      lastName: "",
      email: "",
      birthDay: new Date(Date.now())
    };
  }
}
