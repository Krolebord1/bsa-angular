import { Component, OnInit } from "@angular/core";
import { CrudService } from "../../services/common/crud.service";

@Component({
  template: ''
})
export abstract class ModelsListComponent<TReadDto extends { id:number }> implements OnInit {
  models: TReadDto[] = [];

  modelsService!: CrudService<TReadDto, any>;

  abstract getModelsService(): CrudService<TReadDto, any>;

  ngOnInit(): void {
    this.modelsService = this.getModelsService();
    this.loadModels();
  }

  loadModels(): void {
    this.modelsService.getAll().subscribe(
      response => {
        if(!response.body)
          return;

        this.models = response.body;
      }
    );
  }

  delete(modelDto: TReadDto) {
    this.modelsService.delete(modelDto.id).subscribe(
      response => {
        if(!response.ok)
          return;

        const index = this.models.findIndex(model => model.id == modelDto.id);

        if(index < 0)
          return;

        this.models.splice(index, 1);
      }
    );
  }
}
