import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ComponentCanDeactivate } from "../../guards/can-deactivate.guard";
import { FormGroup } from "@angular/forms";

@Component({
  template: ''
})
export abstract class FormComponent<TWriteDto> implements OnInit, ComponentCanDeactivate {
  @Input() writeDto: TWriteDto = this.getDefaultDto();
  @Output() onCancel = new EventEmitter();
  @Output() onSave = new EventEmitter<TWriteDto>();

  abstract form: FormGroup;

  canDeactivate(): boolean {
    if(this.form.dirty) {
      return confirm("Are you sure to exit?")
    }
    return true;
  }

  ngOnInit(): void {
    this.form.patchValue(this.writeDto);
  }

  abstract getDefaultDto(): TWriteDto;

  save(): void {
    if(!this.form.valid)
      return;

    this.form.markAsPristine();
    this.onSave.emit(this.form.value as TWriteDto);
  }
}
