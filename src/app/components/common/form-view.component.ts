import { Component, ViewChild } from "@angular/core";
import { ComponentCanDeactivate } from "../../guards/can-deactivate.guard";
import { TeamsFormComponent } from "../teams/teams-form/teams-form.component";

@Component({
  template: ''
})
export abstract class FormViewComponent<TFormComponent extends ComponentCanDeactivate> implements ComponentCanDeactivate {
  @ViewChild('form') teamsForm?: TFormComponent;

  t() {
    return TeamsFormComponent;
  }

  canDeactivate(): boolean {
    return this.teamsForm?.canDeactivate() ?? true;
  }
}
