import { Component } from '@angular/core';
import { ModelsListComponent } from "../../common/models-list.component";
import { CrudService } from "../../../services/common/crud.service";
import { TasksService } from "../../../services/tasks/tasks.service";
import { TaskReadDto } from "../../../models/tasks/task-read.dto";

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: [
    './tasks-list.component.scss',
    '../../../styles/center-card.scss',
    '../../../styles/card-list.scss'
  ]
})
export class TasksListComponent extends ModelsListComponent<TaskReadDto> {
  constructor(private tasksService: TasksService) {
    super();
  }

  getModelsService(): CrudService<TaskReadDto, any> {
    return this.tasksService;
  }
}
