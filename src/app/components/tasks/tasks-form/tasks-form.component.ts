import { Component } from '@angular/core';
import { FormComponent } from "../../common/form.component";
import { TaskWriteDto } from "../../../models/tasks/task-write.dto";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TaskStateEnum } from "../../../models/tasks/task-state.enum";
import { Observable } from "rxjs";
import { UserReadDto } from "../../../models/users/user-read.dto";
import { ProjectReadDto } from "../../../models/projects/project-read.dto";
import { ProjectsService } from "../../../services/projects/projects.service";
import { UsersService } from "../../../services/users/users.service";
import { filter, map } from "rxjs/operators";

@Component({
  selector: 'app-tasks-form',
  templateUrl: './tasks-form.component.html',
  styleUrls: ['./tasks-form.component.scss', '../../../styles/form.scss']
})
export class TasksFormComponent extends FormComponent<TaskWriteDto> {
  form: FormGroup = this.formBuilder.group({
    name: [this.writeDto.name, [Validators.required, Validators.maxLength(32)]],
    description: [this.writeDto.description, Validators.required],
    state: [this.writeDto.state, Validators.required],
    finishedAt: [this.writeDto.finishedAt],
    projectId: [this.writeDto.projectId, Validators.required],
    performerId: [this.writeDto.performerId]
  });

  states = TaskStateEnum;

  projects$!: Observable<ProjectReadDto[]>;
  users$!: Observable<UserReadDto[]>;

  constructor(
    private formBuilder: FormBuilder,
    private projectsService: ProjectsService,
    private usersService: UsersService
  ) {
    super();
  }

  override ngOnInit() {
    super.ngOnInit();
    this.projects$ = this.projectsService.getAll().pipe(
      filter(response => response.ok),
      map(value => value.body!)
    );
    this.users$ = this.usersService.getAll().pipe(
      filter(response => response.ok),
      map(value => value.body!)
    );
  }

  getDefaultDto(): TaskWriteDto {
    return {
      name: "",
      description: "",
      state: TaskStateEnum.planned,
      projectId: undefined!
    };
  }

  selectState(value: TaskStateEnum) {
    this.form.patchValue({
      state: value
    });
  }
}
