import { Component } from '@angular/core';
import { FormViewComponent } from "../../common/form-view.component";
import { Router } from "@angular/router";
import { TaskWriteDto } from "../../../models/tasks/task-write.dto";
import { TasksService } from "../../../services/tasks/tasks.service";
import { TasksFormComponent } from "../tasks-form/tasks-form.component";

@Component({
  selector: 'app-tasks-add',
  templateUrl: './tasks-add.component.html',
  styleUrls: ['./tasks-add.component.scss', '../../../styles/center-card.scss']
})
export class TasksAddComponent extends FormViewComponent<TasksFormComponent> {
  constructor(
    private router: Router,
    private tasksService: TasksService
  ) {
    super();
  }

  goBack(): void {
    this.router.navigateByUrl('tasks');
  }

  add(taskDto: TaskWriteDto): void {
    this.tasksService.create(taskDto)
      .subscribe(
        response => {
          if(response.ok)
            this.goBack();
        }
      );
  }
}
