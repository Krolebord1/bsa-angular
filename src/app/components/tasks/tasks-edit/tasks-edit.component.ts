import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormViewComponent } from "../../common/form-view.component";
import { TaskWriteDto } from "../../../models/tasks/task-write.dto";
import { TasksService } from "../../../services/tasks/tasks.service";
import { TaskReadDto } from "../../../models/tasks/task-read.dto";
import { TasksFormComponent } from "../tasks-form/tasks-form.component";

@Component({
  selector: 'app-tasks-edit',
  templateUrl: './tasks-edit.component.html',
  styleUrls: ['./tasks-edit.component.scss', '../../../styles/center-card.scss']
})
export class TasksEditComponent extends FormViewComponent<TasksFormComponent> implements OnInit {
  task: TaskReadDto | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tasksService: TasksService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getTask();
  }

  getTask(): void {
    const routeId = this.route.snapshot.paramMap.get('id');

    if(!routeId) {
      this.task = null;
      return;
    }

    const id: number = +routeId;
    this.tasksService.getById(id).subscribe(
      response => this.task = response.body
    );
  }

  goBack(): void {
    this.router.navigateByUrl("tasks");
  }

  save(taskDto: TaskWriteDto): void {
    if(!this.task) {
      this.goBack();
      return;
    }

    this.tasksService.update(this.task.id, taskDto).subscribe(
      response => {
        if(response.ok) {
          this.goBack();
        }
      }
    );
  }
}
