import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppbarComponent } from './components/appbar/appbar.component';
import { TeamsModule } from "./modules/teams.module";
import { MatToolbarModule } from "@angular/material/toolbar";
import { UsersModule } from "./modules/users.module";
import { CanDeactivateGuard } from "./guards/can-deactivate.guard";
import { ProjectsModule } from "./modules/projects.module";
import { TasksModule } from "./modules/tasks.module";
import { HomeComponent } from './components/home/home.component';
import { MatCardModule } from "@angular/material/card";
import { MatListModule } from "@angular/material/list";

@NgModule({
  declarations: [
    AppComponent,
    AppbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    TeamsModule,
    UsersModule,
    ProjectsModule,
    TasksModule,

    MatToolbarModule,
    MatCardModule,
    MatListModule
  ],
  providers: [
    CanDeactivateGuard
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
